# README #

CIS 554-M401 Object Oriented Programming in C++
Syracuse University
Vaccinator - Final Project
3/25/2021

---

For the final project, the Vaccinator game was created. This game
is based off the popular "Space Invaders" aracde game, and has 
been adjusted to reflect the current pandemic climate. The game
sets up the player as a syringe image/graphic that shoots vaccine
drops at incomming viruses. The goal is for the player to eliminate as 
many viruses as possible without letting them pass through.
Each case of the virus that passes through ends up taking the lives of 1 billion 
people. Since there are ~7 billion people, the player has 7 lives. Once they run 
out the game will display the final score and allow the player to quit.

![Vaccinator](images/vaccinator.png)

References:

- https://www.cplusplus.com/doc/tutorial/polymorphism/
- https://www.sfml-dev.org/learn.php
- https://www.youtube.com/watch?v=BySDfVNljG8&list=PL6xSOsbVA1eb_QqMTTcql_3PdOiE928up