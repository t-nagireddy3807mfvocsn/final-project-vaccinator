#include "Vaccine.h"

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Vaccine.cpp
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Vaccine implementation
// This cpp file contains the vaccine class (represents a vaccine drop
// shot by the player) implementation and it houses all of the member 
// function declarations required set up the movement speed and direction, 
// apart from creating and displaying the vaccine drop object itself.
///////////////////////////////////////////////////////////

// default constructor
Vaccine::Vaccine()
    : movement_speed{ 0.f }
{}

// constructor setting multiple properties of the vaccine drop
Vaccine::Vaccine(
    sf::Texture* texture, // setup image/graphic pointer
    float position_x, // position of the vaccine drop in the x-axis
    float position_y, // position of the vaccine drop in the y-axis
    float direction_x, // move direction of the vaccine drop in the x direction
    float direction_y, // move direction of the vaccine drop in the y direction
    float mov_spd) // set the movement speed of the vaccine drop
{
    shape.setPosition(position_x, position_y); // setting the position of the vaccine drop
    direction.x = direction_x; // setting the x-axis direction of the vaccine drop
    direction.y = direction_y; // setting the y-axis direction of the vaccine drop
    movement_speed = mov_spd; // setting the movement speed of the vaccine drop
    shape.setTexture(*texture); // setting the image/graphic of the vaccine drop

    // setting the size of the vaccine drop object relative to the original image/graphic
    shape.setScale(Vector2f(0.05f, 0.05f)); 
}

// destructor
Vaccine::~Vaccine() {}

// get the boundaries of the vaccine drop
sf::FloatRect Vaccine::getBounds() const
{
    return shape.getGlobalBounds();
}

// move the vaccine drop in a certain direction at the set speed
void Vaccine::update()
{
    // multiply the speed and direction to move the vaccine drop object
    shape.move(movement_speed * direction);
}

// display the vaccine drop onto the game window
void Vaccine::render(sf::RenderTarget* target)
{
	target->draw(shape);
}
