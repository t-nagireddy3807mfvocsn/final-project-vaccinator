#pragma once

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Enemy.h
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Enemy interface
// This header file contains the enemy class (represents a virus)
// declarations along with the declarations of other user-specified 
// viruses that are derived from the base enemy class. The member
// function declarations setup a virus that can be eliminated by the player.
///////////////////////////////////////////////////////////

#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <memory>
using std::shared_ptr;
using std::make_shared;
using std::exception;
using std::cout;
using std::string;

// represents a virus
class Enemy
{
public:

	Enemy(); // default constructor
	virtual ~Enemy(); // destructor

	void initSprite(); // initialiize the virus image and position
	void initTexture(); // setup the image / graphic file

	void update(); // update the virus state (move virus in game window)
	void render(sf::RenderTarget& target); // display virus on game window
	
	// set the position of the virus graphic in window 
	void setPosition(float position_x = 50.f, float position_y = 50.f); 
	
	// set how fast the virus object or image/graphic should move
	virtual void setSpeed();
	virtual void setColor(); // set the color of the virus
	virtual void setScale(); // setting scale of the virus image/graphic 
	virtual int getPoints(); // provide points gained by player
	virtual string showName(); // display the name for the virus type
	virtual Enemy* copy(); // create a new virus instance of the same type

	// get the bounds for the virus image/graphic
	sf::FloatRect getBounds() const;

	// creating a virus and it's image/graphic objects
	sf::Sprite sprite; 
	sf::Texture texture;
	
	float posX; // x-axis position of the virus in game window
	float posY; // y-axis position of the virus in game window
	
private:
	
	int points = 10; // points gained by player
};

///////////////////////////////////////////////////////////
// Class Influenza interface
///////////////////////////////////////////////////////////

class Influenza : public Enemy
{
public:
	
	Influenza(); // default constructor
	Influenza* copy(); // create copy of this virus instance
	void setSpeed(); // move virus towards player at a certain speed
	string showName(); // display the type of virus
	void setColor(); // set the color of the virus
	void setScale(); // set the size of the virus relative to the original size of the image
	int getPoints(); // retrieve the points gained for eliminating this virus

private:
	
	int points = 1; // points gained by player when this virus is successfully eliminated
};

///////////////////////////////////////////////////////////
// Class Covid interface
///////////////////////////////////////////////////////////

class Covid : public Enemy
{
public:
	
	Covid(); // default constructor
	Covid* copy(); // create copy of this virus instance
	void setSpeed(); // move virus towards player at a certain speed
	string showName(); // display the type of virus
	void setColor(); // set the color of the virus
	void setScale(); // set the size of the virus relative to the original size of the image
	int getPoints(); // retrieve the points gained for eliminating this virus

private:
	
	int points = 2; // points gained by player when this virus is successfully eliminated
};

///////////////////////////////////////////////////////////
// Class Pneumonia interface
///////////////////////////////////////////////////////////

class Pneumonia : public Enemy
{
public:
	
	Pneumonia(); // default constructor
	Pneumonia* copy(); // create copy of this virus instance
	void setSpeed(); // move virus towards player at a certain speed
	string showName(); // display the type of virus
	void setColor(); // set the color of the virus
	void setScale(); // set the size of the virus relative to the original size of the image
	int getPoints(); // retrieve the points gained for eliminating this virus

private:
	
	int points = 3; // points gained by player when this virus is successfully eliminated
};

///////////////////////////////////////////////////////////
// Class <SOME_VIRUS> interface
///////////////////////////////////////////////////////////


/*
derived enemy class to be added - another virus or covid variant
*/
