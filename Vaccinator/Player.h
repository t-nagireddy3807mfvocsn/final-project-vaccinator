#pragma once

#include "Vaccine.h"

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Player.h
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Player interface
// This header file contains the player class (represents a player
// in the Vaccinator game) interface and it houses all of the member 
// function declarations required by the player to attack, and keep score 
// in the game, apart from creating and displaying the player object itself.
///////////////////////////////////////////////////////////

// represents the game user/player
class Player
{

public:

	Player(); // constructor
	virtual ~Player(); // destructor
	void update(); // update the state of attack cooldown for the player
	void render(sf::RenderTarget& target); // display the player object in the game window

	// move the player object in a specific direction
	void move(const float direction_x, const float direction_y); 

	// retrieve the player positon values (x and y axis values)
	const Vector2f& getPos() const;
	
	int getHp(); // retrieve the hit points/health/life
	void reduceHp(int hp_); // decrement the health/life by the passed value
	int getPoints(); // retrieve the points accrued by the player

	// pass in the points to be gained by the player, and 
	// add this to the existing number of points
	void setPoints(int point_);

	float attackCooldown; // cooldown for vaccine drops to shoot

	// max value for when player can attack if cooldown above is larger than this value
	float attackCooldownMax; 
	bool attackAllowed(); // check if player can attack

	// set the position of the player based on the vector value passed
	void setPosition(const Vector2f position);
	
	// set the position of the player based on the values passed
	void setPosition(const float x, const float y);

	// get the player object boundaries; image/graphic boundaries
	sf::FloatRect getBounds() const;

private:

	sf::Sprite sprite;  // setup player object
	sf::Texture texture; // setup image/graphic

	// initializeing the player object and size
	void initSprite();

	// setting the imgae/graphic of the player to be displayed onto the game window
	void initTexture();

	// increment the cooldown value when the state of the player is updated; and
	// while it is smaller than the max cooldown value 
	void updateCooldown();

	float movement_speed; // player object's movement speed; sensitivity to keyboard presses
	int hp; // hit points/health/life
	int points; // accrued points by player; incremented upon eliminating a virus 
};