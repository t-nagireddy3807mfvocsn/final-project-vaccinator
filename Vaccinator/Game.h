#pragma once

#include "Player.h"
#include "Enemy.h"

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Game.h
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Game interface
// This header file contains the game class (represents a Vaccinator game)
// declarations. It houses all of the member function declarations required 
// to run the game, through creating/updating the 
// game objects (player, vaccine drops, viruses), and 
// displaying them onto the console window.
///////////////////////////////////////////////////////////


class Game
{

public:

	// initialize the game; default constructor
	Game();

	//  freeing memory of the game and it's elements (window, etc.)
	virtual ~Game();

	// update what's happening in the game
	void update();

	// display the updates/changes made by the player or in the games
	void render();

	// close or escape game application
	void updatePollEvents();

	// update the game input
	void updateInput();

	// update vaccine drops
	void updateVaccine();

	// spawn and delete enemies from memoery depending on the state in the game
	void updateEnemies();

	// check if the player has eliminated the virus
	void updateCombat();

	// check the player position; if the edges of the game window, prevent them from going further out of the game
	// console window
	void updateCollision();

	// storing the enemy types passed from Main.cpp to a vector
	void setEnemyTypes(vector<Enemy*> enemy_types_);

	// game to check this value to continue to stay open
	const bool isopen() const;

	// retrieve the health/life of the player
	int getPlayerHealth();

	// retrieve the final score that the player has achieved
	int getScore();

private:

	// initialize game console window and properties
	void initWindow();

	// create enemy entities
	void initEnemies();

	// setting up the image/graphic for the vaccine/drop
	void initTextures();

	// setup a new player for this game session
	void initPlayer();

	// window pointer
	sf::RenderWindow* window;

	// setting up a event resembling a game operation
	sf::Event event;

	// setup vidio mode access
	sf::VideoMode video_mode;

	// position of the mouse
	Vector2i mousePositionWindow;

	// player instance 
	Player* player;

	// the timer to which the virus will be created once it equals the
	// max timer value
	float spawnTimer;
	float spawnTimerMax;

	vector<Enemy*> enemy_types; // store types of viruses
	vector<Enemy*> enemies; // store viruses

	// storing image/graphic for the vaccine drop
	map<string, sf::Texture*> textures;

	// storing vaccine drops created
	vector<Vaccine*> drops;

};

