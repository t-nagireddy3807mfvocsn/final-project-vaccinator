#include "Enemy.h"

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Enemy.cpp
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Enemy implementation
// This cpp file contains the enemy class (represents a virus)
// implementation along with the implementation of other user-specified 
// viruses that are derived from the base enemy class. The member
// functions setup a virus that can be eliminated by the player.
///////////////////////////////////////////////////////////

// default constructor
Enemy::Enemy()
    : posX{ 50.f } // initial position x-axis
    , posY{ 50.f } // initial position y-axis
{
    initTexture(); // initialize virus image/graphic
    initSprite(); // initialize the virus
    setScale(); // initialize the size of the virus
    setColor(); // set the color of the virus
    setSpeed(); // set the movement speed of the virus
    setPosition(posX, posY); // set the position (x and y axis)
}

// destructor
Enemy::~Enemy() {}

// initialiize the virus image and position
void Enemy::initSprite()
{
    sprite.setTexture(texture); // set the image graphic for the virus object
    sprite.setPosition(sf::Vector2f(50.f, 50.f)); // set the initial position for the virus
}

// update the virus state in the game
void Enemy::update()
{
    setSpeed(); // moves the virus in the window
}

// display the virus to the game window
void Enemy::render(sf::RenderTarget& target)
{
	target.draw(sprite); // display virus image/graphic in window
}

// set the position of the virus graphic in window
void Enemy::setPosition(float position_x, float position_y)
{
    // pass in x and y axis position float values to set the position of
    // the virus object
    sprite.setPosition(position_x, position_y); 
}

// set how fast the virus object or image/graphic should move in game window
void Enemy::setSpeed()
{
    sprite.move(0.f, 1.f); // move virus in the y direction (down) at a certain speed
}

// set the color of the virus
void Enemy::setColor()
{
    sprite.setColor(sf::Color::Black); // base virus is set to Black; will change for others
}

// set the size of the virus image/graphic with respect to the original image (which is 1)
void Enemy::setScale()
{
    sprite.setScale(sf::Vector2f(0.2f, 0.2f)); // setting scale of the virus image/graphic 
}

// provide the points to add when the player eliminates the virus
int Enemy::getPoints()
{
    return points; // provide points gained by player
}

// get the bounds for the virus image/graphic
sf::FloatRect Enemy::getBounds() const
{
    return sprite.getGlobalBounds();
}

// display the name for the virus type
string Enemy::showName()
{
    return "base_virus";
}

// create a copy of this virus instance, and return a pointer
Enemy* Enemy::copy()
{
    return new Enemy; // create a new virus instance of the same type 
}

// setup the image/graphic file for the virus that would be displayed
// in the game window
void Enemy::initTexture()
{
    // load texture from file
    try
    {
        // attempt to load the image/graphic from file
        texture.loadFromFile("Textures/virus.png");
    }
    // if loading fails, display error message
    catch (exception e)
    {
        cout
            << "Loading virus image file failed :"
            << e.what()
            << "\n"
            ;
    }
}


///////////////////////////////////////////////////////////
// Class Pneumonia implementation
///////////////////////////////////////////////////////////

// default constructor
Pneumonia::Pneumonia() {}

// create a new instance of the virus
// as multiple instances with different positions
// are displayed on the game window
Pneumonia* Pneumonia::copy()
{
    return new Pneumonia;
}

// sets the movement speed of the virus
void Pneumonia::setSpeed()
{
    // move the virus by a specific amount in the y direction
    // once the update member function is called
    sprite.move(0.f, 1.f); 
}

// display the name of the virus type
string Pneumonia::showName()
{
    return "Pnuemonia";
}

// set the color of the virus image/graphic that is displayed
void Pneumonia::setColor()
{
    sprite.setColor(sf::Color::Yellow);
}

// set the scale of the virus; alter the size based on a scale of 1
// for the original image
void Pneumonia::setScale()
{
    sprite.setScale(sf::Vector2f(0.08f, 0.08f));
}

// retrieve points that the player would gain upon eliminating this virus
int Pneumonia::getPoints()
{
    return points;
}


///////////////////////////////////////////////////////////
// Class Covid implementation
///////////////////////////////////////////////////////////

// default constructor
Covid::Covid() {}

// create a new instance of the virus
// as multiple instances with different positions
// are displayed on the game window
Covid* Covid::copy()
{
    return new Covid;
}

// sets the movement speed of the virus
void Covid::setSpeed()
{
    // move the virus by a specific amount in the y direction
    // once the update member function is called
    sprite.move(0.f, 1.f); 
}

// display the name of the virus type
string Covid::showName()
{
    return "Covid";
}

// set the color of the virus image/graphic that is displayed
void Covid::setColor()
{
    sprite.setColor(sf::Color::Red);
}

// set the scale of the virus; alter the size based on a scale of 1
// for the original image
void Covid::setScale()
{
    sprite.setScale(sf::Vector2f(0.02f, 0.02f));
}

// retrieve points that the player would gain upon eliminating this virus
int Covid::getPoints()
{
    return points;
}


///////////////////////////////////////////////////////////
// Class Influenza implementation
///////////////////////////////////////////////////////////

// default constructor
Influenza::Influenza() {}

// create a new instance of the virus
// as multiple instances with different positions
// are displayed on the game window
Influenza* Influenza::copy()
{
    return new Influenza;
}

// sets the movement speed of the virus
void Influenza::setSpeed()
{
    // move the virus by a specific amount in the y direction
    // once the update member function is called
    sprite.move(0.f, 2.f); 
}

// display the name of the virus type
string Influenza::showName()
{
    return "Influenza";
}

// set the color of the virus image/graphic that is displayed
void Influenza::setColor()
{
    sprite.setColor(sf::Color::Green);
}

// set the scale of the virus; alter the size based on a scale of 1
// for the original image
void Influenza::setScale()
{
    sprite.setScale(sf::Vector2f(0.05f, 0.05f));
}

// retrieve points that the player would gain upon eliminating this virus
int Influenza::getPoints()
{
    return points;
}


///////////////////////////////////////////////////////////
// Class <SOME_VIRUS> implementation
///////////////////////////////////////////////////////////


/*
derived enemy class to be added - another virus 
*/