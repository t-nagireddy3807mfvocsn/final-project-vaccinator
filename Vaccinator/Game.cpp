#include "Game.h"

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Game.cpp
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Game implementation
// This cpp file contains the game class (represents a Vaccinator game)
// implementation. It houses all of the member functions required to
// run the game, through creating/updating the 
// game objects (player, vaccine drops, viruses), and 
// displaying them onto the console window.
///////////////////////////////////////////////////////////

// setting up a brand new game
Game::Game()
    : event{}, window{ nullptr }
{
	initWindow(); // setting up the game console window
    initTextures(); // setting up the image/graphic for the vaccine/drop
    initPlayer(); // initialize the player
    initEnemies(); // setup the viruses for the player to shoot down
}

// freeing memory from the game class by removing game objects stored in
// memory after game has ended
Game::~Game()
{
    // remove instances of the game window and player
    delete window;
    delete player;
   
    // remove any images/graphics stored
    for (auto &i : textures)
    {
        delete i.second;
    }

    // remove all instances of the vaccine drops shot by
    // the player
    for (auto *i : drops)
    {
        delete i;
    }

    // remove all instances of the enemy created
    for (auto *i : enemies)
    {
        delete i;
    }
}

// setting up the game console window
void Game::initWindow()
{
	// set height and width for the game console
	video_mode.height = 640;
	video_mode.width = 640;

	// setup the console window for the game
	window = new sf::RenderWindow(video_mode, "Vaccinator", sf::Style::Titlebar | sf::Style::Close);

    // set the frame rate of the game to be 144 Hz (frames per second)
    window->setFramerateLimit(144);

    // 
    window->setVerticalSyncEnabled(false);
}

// setup a new player for this game session
void Game::initPlayer()
{
    player = new Player; // creating a new player instance in memory

    // set the initial position of the player to be at the bottom of the screen
    player->setPosition(0.f, 600.f); 
}

// initializing the frequency at which viruses should appear in the game
void Game::initEnemies()
{
    spawnTimerMax = 50.f; // setting the spawn timer to an initial value

    // setting the spawntimer to the max initially; will be reset once max is hit
    spawnTimer = spawnTimerMax; 
}

// setting close and escape key press options for the game
void Game::updatePollEvents()
{
    // user is entering keys in the game
    while(window->pollEvent(event))
    {
        // the user wants to close the application by
        // hitting the red "X" in the top right of the game window
        if (event.type == sf::Event::Closed)
        {
            window->close(); // close the game application
            break;
        }

        // if the user presses a key
        else if (event.type == sf::Event::KeyPressed)
        {
            // if the user hits the escape key
            if (event.key.code == sf::Keyboard::Escape)
            {
                window->close(); // close the game application
                break;
            }
        }
    }
}

// setting up the image/graphic for the vaccine/drop
void Game::initTextures()
{
    textures["Vaccine"] = new sf::Texture(); // setting a new texture object for the vaccine drop
    textures["Vaccine"]->loadFromFile("Textures/drop.png"); // loading the image/graphic from file
}

// check keyboard user/player inputs and move the syringe accordingly
void Game::updateInput()
{
    // move syringe left along the x-axis when letter "a" is pressed
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        player->move(-1.f, 0.f);
    }
    // move syringe right along the x-axis when letter "d" is pressed
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        player->move(1.f, 0.f);
    }
    // move syringe up along the y-axis when letter "w" is pressed
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        player->move(0.f, -1.f);
    }
    // move syringe down along the y-axis when letter "s" is pressed
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        player->move(0.f, 1.f);
    }

    // if the left mouse button is pressed and the player can attack
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && player->attackAllowed())
    {
        drops.push_back(new Vaccine(
            textures["Vaccine"], 
            (player->getPos().x)+18.f,
            (player->getPos().y)+8.f,
            0.f,
            -1.f,
            2.f
        ));
    }
}

// create the vaccine drops based on the mouse clicks from the user
void Game::updateVaccine()
{
    // counter for identifying the location to remove the vaccine object in vector
    int counter = 0;

    for (auto* vaccine : drops)
    {
        vaccine->update(); // move the vaccine up along the y-axis in the game console window

        // delete and remove vaccine once it passes the top of the screen
        if (vaccine->getBounds().top + vaccine->getBounds().height < 0.f)
        {
            delete drops.at(counter);
            drops.erase(drops.begin() + counter);

            // decrement counter
            --counter;
        }

        // increment counter
        ++counter;
    }
}

// spawn and delete enemies from memoery depending on the state in the game
void Game::updateEnemies()
{
    // incrementing the spawntimer
    spawnTimer += 0.4f;

    // spawning enemies if the incremented spawnTimer is greater than or equal to the max set value
    if (spawnTimer >= spawnTimerMax)
    {
        // randomizing the type of virus that spawns
        int random_enemy = rand() % enemy_types.size();

        // replicating the virus by calling the clone member function
        Enemy* ptr = enemy_types[random_enemy]->copy();
        
        // setting the properties of the virus - color, size, and speed
        ptr->setPosition(rand() % window->getSize().x - 20.f, -100.f);
        //ptr->setColor(); // set the color of the virus
        //ptr->setScale(); // set the size of the virus with respect to the original image/graphic
        enemies.push_back(ptr); // store virus in vector

        // setting the spawn timer back to zero
        spawnTimer = 0.f;
    }

    // updating enemies
    int counter = 0;

    for (auto *enemy : enemies)
    {
        // move enemy in game window
        enemy->update();

        // check if the bounds of the virus and bottom of the game window intersect
        if (enemy->getBounds().top + enemy->getBounds().height > window->getSize().y)
        {
            // remove virus
            delete enemies.at(counter);
            enemies.erase(enemies.begin() + counter);
            
            // decrement total number of viruses
            --counter;

            // reduce the life/health of the player
            player->reduceHp(1);

            // display message showing remaining life/health of the player
            cout
                << "Remaining human population (billion) -- "
                << player->getHp()
                << "\n"
                ;
        }

        // increment counter
        ++counter;
    }
}

// check if the player has eliminated the virus
void Game::updateCombat()
{
    // checking each virus created
    for (int i = 0; i < enemies.size(); i++)
    {
        // initially set if the enemy has been terminated to false
        bool enemy_terminated = false;

        // remove virus only if it hasn't been deleted yet
        for (size_t k = 0; k < drops.size() && enemy_terminated == false; k++)
        {
            // check the bounds of the virus and vaccine drop intersect
            if (enemies[i]->getBounds().intersects(drops[k]->getBounds()))
            {
                // player/syringe gains points for virus eliminated; amount of
                // points is based on the type of virus
                player->setPoints(enemies[i]->getPoints());

                // remove virus
                delete enemies[i];
                enemies.erase(enemies.begin() + i);

                // remove vaccine drop
                delete drops[k];
                drops.erase(drops.begin() + k);

                // record that virus has been eliminated
                enemy_terminated = true;

            }
        }
    }
}

// check the player position; if the edges of the game window, prevent them from going further out of the game
// console window
void Game::updateCollision()
{
    // once syringe/player is at the left window screen, prevent them from going further out of game window
    if (player->getBounds().left < 0.f)
    {
        player->setPosition(0.f, player->getBounds().top);
    }

    // once syringe/player is at the right window screen, prevent them from going further out of game window
    else if (player->getBounds().left + player->getBounds().width >= window->getSize().x)
    {
        player->setPosition(window->getSize().x - player->getBounds().width, player->getBounds().top);
    }

    // once syringe/player is at the top window screen, prevent them from going further out of game window
    if (player->getBounds().top < 0.f)
    {
        player->setPosition(player->getBounds().left, 0.f);
    }

    // once syringe/player is at the bottom window screen, prevent them from going further out of game window
    else if (player->getBounds().top + player->getBounds().height >= window->getSize().y)
    {
        player->setPosition(player->getBounds().left, window->getSize().y - player->getBounds().height);
    }
}

// storing the enemy types passed from Main.cpp to a vector
void Game::setEnemyTypes(vector<Enemy*> enemy_types_)
{
    // storing the virus types locally
    enemy_types = enemy_types_;
}

// update what's happening in the game
void Game::update()
{
    // update the state of the player
    player->update();

    // call method that updates the game operation
    updatePollEvents();

    // keyboard inputs
    updateInput();

    // check the player/syringe position at the edges of the game window
    updateCollision();

    // create the vaccine drops 
    updateVaccine();
    
    // create the viruses
    updateEnemies();

    // check if the player has eliminated the virus
    updateCombat();

}

// display the updates/changes made by the player or in the game
void Game::render()
{
    // clear previous game frame
    window->clear(sf::Color(255, 255, 255));

    // draw in game console
    //window->draw(enemy);

    player->render(*window); // draw player
    
    // display vaccine drops shot by player on to the game window
    for (auto* vaccine_drop : drops)
    {
        vaccine_drop->render(window);
    }

    // display viruses on to the game window
    for (auto* enemy : enemies)
    {
        enemy->render(*window);
    }

    // render the updated/new frame of the state of the game on the console window
    window->display();
}

// value signifying the game is running
const bool Game::isopen() const
{
	return window->isOpen();
}

// retrieve the health/life of the player
int Game::getPlayerHealth()
{
    return player->getHp();
}

// retrieve the final score that the player has achieved
int Game::getScore()
{
    return player->getPoints();
}