#pragma once

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Vaccine.h
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Vaccine interface
// This header file contains the vaccine class (represents a vaccine drop
// shot by the player) declarations and it houses all of the member 
// function declarations required set up the movement speed and direction, 
// apart from creating and displaying the vaccine drop object itself.
///////////////////////////////////////////////////////////

// standard libraries
#include <iostream>
#include <map>
#include <string>
#include <iomanip>

// simple and fast multimedia library (sfml) libraries
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>

using std::cout;
using std::cin;
using sf::Vector2f;
using sf::Vector2i;
using std::exception;
using std::map;
using std::string;
using std::vector;
using std::setw;

// represents a vaccine drop that the player shoots at viruses
class Vaccine
{
public:

	Vaccine(); // default constructor
	~Vaccine(); // destructor
	
	// constructor setting multiple properties of the vaccine drop
	Vaccine(
		sf::Texture* texture, // setup image/graphic pointer
		float position_x, // position of the vaccine drop in the x-axis
		float position_y, // position of the vaccine drop in the y-axis
		float direction_x, // move direction of the vaccine drop in the x direction
		float direction_y, // move direction of the vaccine drop in the y direction
		float mov_spd); // set the movement speed of the vaccine drop

	sf::FloatRect getBounds() const; // get the boundaries of the vaccine drop

	void update(); // move the vaccine drop in a certain direction at the set speed
	void render(sf::RenderTarget* target); // display the vaccine drop onto the game window

private:

	sf::Sprite shape; // setup the vaccine drop object
	Vector2f direction; // store the direction x and y axis values that the vaccine drop would move in
	float movement_speed; // set the movement speed of the vaccine drop

};

