
/*
Tanay Nagireddy
CIS 554-M401 Object Oriented Programming in C++
Syracuse University
Vaccinator - Final Project
3/25/2021

For the final project, the Vaccinator game was created. This game
is based off the popular "Space Invaders" aracde game, and has 
been adjusted to reflect the current pandemic climate. The game
sets up the player as a syringe image/graphic that shoots vaccine
drops at incomming viruses. The goal is for the player to eliminate as 
many viruses as possible without letting them pass through.
Each case of the virus that passes through ends up taking the lives of 1 billion 
people. Since there are ~7 billion people, the player has 7 lives. Once they run 
out the game will display the final score and allow the player to quit.

References:
https://www.cplusplus.com/doc/tutorial/polymorphism/
https://www.sfml-dev.org/learn.php
https://www.youtube.com/watch?v=BySDfVNljG8&list=PL6xSOsbVA1eb_QqMTTcql_3PdOiE928up
*/

#include "Game.h"

int main()
{
    // setting up a brand new game and
    // pass in vector with enemy types
    Game game;

    // setup vector of Enemy base class pointers
    vector<Enemy*> enemy_types;
    
    // setup enemy type objects
    Influenza influenza;
    Covid covid;
    Pneumonia pneumonia;

    // fill vector with enemy types (as addresses)
    enemy_types.push_back(&influenza);
    enemy_types.push_back(&covid);
    enemy_types.push_back(&pneumonia);
    
    /*
    * add custom enemy derived class (virus)
    * enemy_types.push_back(&<enemy derived class>);
    */
    
    // pass all enemy types to the game class
    game.setEnemyTypes(enemy_types);

    // seed random to get random values for spawning random enemies
    srand(time(0));
    
    // run the game only while player's health is not zero
    while (game.isopen() && game.getPlayerHealth() > 0)
    {
        // update what's happening in the game
        game.update();

        // render the new game frame with all of the objects (vaccine drops, enemies, syringe)
        game.render();
    }

    // once the player is defeated, display final score and quit game
    cout << "\nFinal Score: " << game.getScore() << "\n";
    cout << "\n### Game Over ###\n\n";
    cout 
        << "Press any key followed by the enter key"
        << " (or just hit the enter key) to quit..." << std::endl;
    
    // hold open the console window until the player presses some key followed by enter
    // or just the enter key
    std::cin.get();

}
