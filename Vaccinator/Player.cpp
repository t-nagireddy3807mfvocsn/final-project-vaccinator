
#include "Player.h"

///////////////////////////////////////////////////////////
// Tanay Nagireddy
// 3/25/2021
// Player.cpp
// CIS 554 - M401 Object Oriented Programming in C++
// Syracuse University
// Vaccinator - Final Project
// Class Player implementation
// This cpp file contains the player class (represents a player
// in the Vaccinator game) implementation and it houses all of the member 
// functions required by the player to attack, and keep score in the game, 
// apart from creating and displaying the player object itself.
///////////////////////////////////////////////////////////

// default constructor
Player::Player()
    : movement_speed{ 5.f } // initalizing the movement speed; sensitivity to keyboard presses
    , attackCooldownMax{ 15.f } // setting max value triggering cooldown for vaccine drops being shot
    , attackCooldown{ 15.f } // setting the initial value for cooldown for which 
    , hp{ 7 } // set the hitpoints/health/life of the player
    , points{ 0 } // setting the points gained by the player to zero initially
{
    initTexture(); // setting up the image/grapic used by the player; syringe.png
    initSprite(); // setting the player object
}

// destructor
Player::~Player() {}

// initializeing the player object and size
void Player::initSprite()
{
    // setup player image/graphic
    sprite.setTexture(texture);

    // resize syringe/player object with respect to it's original image/graphic size
    sprite.scale(0.1f, 0.1f);
}

// setting the imgae/graphic of the player to be displayed onto the game window
void Player::initTexture()
{
    // load texture from file
    try
    {
        // attempt to load file from window
        texture.loadFromFile("Textures/syringe.png");
    }
    // display error message if loading image/graphic fails
    catch (exception e)
    {
        cout
            << "Loading syringe player image file failed :"
            << e.what()
            << "\n"
            ;
    }
}

// check if player can attack
bool Player::attackAllowed()
{
    // reset the attackcooldown if it's greater than or equal to the max value
    if (attackCooldown >= attackCooldownMax)
    {
        attackCooldown = 0.f;
        return true;
    }

    // don't allow the player to attack until condition above has been met
    return false;
}

// increment the cooldown value when the state of the player is updated; and
// while it is smaller than the max cooldown value 
void Player::updateCooldown()
{
    if (attackCooldown < attackCooldownMax)
    {
        attackCooldown += 0.5f;
    }
}

// update the state of the player
void Player::update()
{
    updateCooldown(); // call the cooldown member function
}

// display player to game console window
void Player::render(sf::RenderTarget& target)
{
    target.draw(sprite);
}

// move player in the direction specified by the positon passed by user
void Player::move(const float direction_x, const float direction_y)
{
    sprite.move(movement_speed * direction_x, movement_speed * direction_y);
}

// retrieve the position of the player object in the game console window
const Vector2f& Player::getPos() const
{
    return sprite.getPosition();
}

// retrieve the hit points of the player
int Player::getHp()
{
    return hp;
}

// reduce hit points/life/health of player by amount passed
void Player::reduceHp(int hp_)
{
    hp = hp - hp_;

    // check if health/life is less than zero, and keep it at zero if
    // it is
    if (hp < 0)
    {
        hp = 0;
    }
}

// retrieve the points player has gained
int Player::getPoints()
{
    return points;
}

// pass in the points to be gained by the player, and 
// add this to the existing number of points
void Player::setPoints(int point_)
{
    points = points + point_;
}

// set the position of the player based on the vector value passed
void Player::setPosition(const Vector2f position)
{
    sprite.setPosition(position);
}

// set the position of the player based on the values passed
void Player::setPosition(const float x, const float y)
{
    sprite.setPosition(x, y);
}

// get the player object boundaries; image/graphic boundaries
sf::FloatRect Player::getBounds() const
{
    return sprite.getGlobalBounds();
}
